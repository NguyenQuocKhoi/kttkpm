//package sv.iuh.fit.movie.repositories;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//import sv.iuh.fit.movie.models.Episode;
//
//import java.util.List;
//
//@Repository
//public interface EpisodeRepository extends JpaRepository<Episode, Long> {
//    List<Episode> getEpisodeByMovieId(Long id);
//}
