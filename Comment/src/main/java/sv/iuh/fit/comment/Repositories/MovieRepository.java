package sv.iuh.fit.comment.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sv.iuh.fit.comment.models.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
}
