//package sv.iuh.fit.director.services;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//import redis.clients.jedis.Jedis;
//import sv.iuh.fit.director.models.Director;
//import sv.iuh.fit.director.repositories.DirectorRepository;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.time.LocalDate;
//import java.time.format.DateTimeFormatter;
//import java.util.Locale;
//
//@Service
//public class DirectorServiceImpl implements DirectorService {
//
//    @Autowired
//    private DirectorRepository directorRepository;
//    private Jedis jedis = new Jedis();
//
//
//    @Override
//    public Director findDirectorById(Long id) throws ParseException {
//        String key = String.valueOf(id);
//        if (jedis.exists(key)) {
//            Director directorInCache = new Director();
//            directorInCache.setId(id);
//
//
//            String firstName = jedis.hget(key, "firstName");
//            if (firstName != null) {
//                directorInCache.setFirstName(firstName);
//            }
//            String lastName = jedis.hget(key, "lastName");
//            if (lastName != null) {
//                directorInCache.setLastName(lastName);
//            }
//            String ageStr = jedis.hget(key, "age");
//            if (ageStr != null) {
////                directorInCache.setAge(Integer.parseInt(ageStr));
//                int age = Integer.parseInt(ageStr);
//                directorInCache.setAge(age);
//            }
//            String genderStr = jedis.hget(key, "gender");
//            if (genderStr != null) {
//                Boolean gender = Boolean.parseBoolean(genderStr);
//                directorInCache.setGender(gender);
//            }
//            String email = jedis.hget(key, "email");
//            if (email != null) {
//                directorInCache.setEmail(email);
//            }
//            String phone = jedis.hget(key, "phone");
//            if (phone != null) {
//                directorInCache.setPhone(phone);
//            }
//            String address = jedis.hget(key, "address");
//            if (address != null) {
//                directorInCache.setAddress(address);
//            }
//            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//            String dobStr = jedis.hget(key, "dob");
//            if (dobStr != null) {
//                LocalDate dob = LocalDate.parse(dobStr, formatter);
//                directorInCache.setDob(dob);
//            }
//            return directorInCache;
//        } else {
//            Director director = directorRepository.findById(id).orElse(null);
//            if (director != null) {
//                setDirectorIntoCache(key, director);
//            }
//            return director;
//        }
//    }
//
//    @Override
//    public Director createDirector(Director director) throws ParseException {
////        System.out.println(jedis);
//        Director saveDirector = directorRepository.save(director);
//        String key = String.valueOf((director.getId()));
//        setDirectorIntoCache(key, saveDirector);
//        return saveDirector;
//    }
//
//    @Override
//    public void deleteDirector(Director director) throws ParseException {
//        directorRepository.delete(director);
//        if (jedis.exists(String.valueOf(director.getId()))) {
//            jedis.del(String.valueOf(director.getId()));
//        }
//    }
//
//    @Override
//    public Director updateDirector(Director director, Director newDirector) throws ParseException {
//        if (newDirector.getFirstName() != null) {
//            director.setFirstName(newDirector.getFirstName());
//        }
//        if (newDirector.getLastName() != null) {
//            director.setLastName(newDirector.getLastName());
//        }
//        if (newDirector.getAge() <= 0) {
//            director.setAge(newDirector.getAge());
//        }
//        if (newDirector.getGender() != null) {
//            director.setGender(newDirector.getGender());
//        }
//        if (newDirector.getEmail() != null) {
//            director.setEmail(newDirector.getEmail());
//        }
//        if (newDirector.getPhone() != null) {
//            director.setPhone(newDirector.getPhone());
//        }
//        if (newDirector.getAddress() != null) {
//            director.setAddress(newDirector.getAddress());
//        }
//        if (newDirector.getDob() != null) {
//            director.setDob(newDirector.getDob());
//        }
//        if (jedis.exists(String.valueOf(director.getId()))) {
//            setDirectorIntoCache(String.valueOf(director.getId()), director);
//        }
//        return directorRepository.save(director);
//    }
//
//    private void setDirectorIntoCache(String key, Director director) {
//        if (director.getFirstName() != null) {
//            jedis.hset(key, "firstName", director.getFirstName());
//        }
//        if (director.getLastName() != null) {
//            jedis.hset(key, "lastName", director.getLastName());
//        }
//        if (director.getAge() <= 0) {
//            jedis.hset(key, "age", String.valueOf(director.getAge()));
//        }
//        if (director.getGender() != null) {
//            jedis.hset(key, "gender", director.getGender().toString());
//        }
//        if (director.getEmail() != null) {
//            jedis.hset(key, "email", director.getEmail());
//        }
//        if (director.getPhone() != null) {
//            jedis.hset(key, "phone", director.getPhone());
//        }
//        if (director.getAddress() != null) {
//            jedis.hset(key, "address", director.getAddress());
//        }
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        String dobStr = director.getDob().format(formatter);
//        jedis.hset(key, "dob", dobStr);
//    }
//}
