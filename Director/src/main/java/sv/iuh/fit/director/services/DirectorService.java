//package sv.iuh.fit.director.services;
//
//import sv.iuh.fit.director.models.Director;
//
//import java.text.ParseException;
//
//public interface DirectorService {
//
//    Director findDirectorById(Long id) throws ParseException;
//    Director createDirector(Director director) throws ParseException;
//    void deleteDirector(Director director) throws ParseException;
//    Director updateDirector(Director director, Director newDirector) throws ParseException;
//
//}
