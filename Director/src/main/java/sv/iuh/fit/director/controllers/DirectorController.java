package sv.iuh.fit.director.controllers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import sv.iuh.fit.director.models.Director;
import sv.iuh.fit.director.repositories.DirectorRepository;

import java.util.List;

@RestController
@RequestMapping("/api/directors")
//@AllArgsConstructor
public class DirectorController {

    @Autowired
    private DirectorRepository directorRepository;
//    private final DirectorService directorService;

    @GetMapping("/findMoviesByDirectorId/{directorId}")
    ResponseEntity findMovieByDirectorId(@PathVariable Long directorId) {
        RestTemplate restTemplate = new RestTemplate();
        Object list = restTemplate.getForObject("http://localhost:8082/api/movies/" + directorId.toString(), List.class);
        return ResponseEntity.ok(list);
    }


    @GetMapping("/")
    public List<Director> findAllDirectors() {
        return directorRepository.findAll();
    }

    @GetMapping("/{directorId}")
    public Director findDirectorById(@PathVariable("directorId") long directorId) {
        return directorRepository.findById(directorId).orElse(null);
    }

    @PostMapping("/addDirector")
    public Director addDirector(@RequestBody Director director) {
        return directorRepository.save(director);
    }

    @PutMapping("/updateDirector/{directorId}")
    public ResponseEntity<Director> updateDirector(@PathVariable Long directorId, @RequestBody Director director) {
        Director checkExist = directorRepository.findById(directorId).orElse(null);
//        System.out.println(checkExist);
        if (checkExist == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            checkExist.setFirstName(director.getFirstName());
            checkExist.setLastName(director.getLastName());
            checkExist.setAge(director.getAge());
            checkExist.setGender(director.getGender());
            checkExist.setEmail(director.getEmail());
            checkExist.setPhone(director.getPhone());
            checkExist.setDob(director.getDob());
            directorRepository.save(checkExist);
            return ResponseEntity.ok(checkExist);
        }
    }

    @PostMapping("/deleteDirector/{directorId}")
    public ResponseEntity<String> deleteDirector(@PathVariable("directorId") Long directorId) {
        Director director = directorRepository.findById(directorId).orElse(null);
        if (director == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Director with id " + directorId + " not found");
        } else {
            directorRepository.deleteById(directorId);
            return ResponseEntity.ok("Delete success with director id: " + directorId);
        }
    }


//    @GetMapping("/findMoviesByDirectorId/{directorId}")
//    ResponseEntity findMovieByDirectorId(@PathVariable Long directorId) {
//        RestTemplate restTemplate = new RestTemplate();
//        Object list = restTemplate.getForObject("http://localhost:8082/api/movies/" + directorId.toString(), List.class);
//        return ResponseEntity.ok(list);
//    }



//    @PostMapping("/addDirector")
//    public ResponseEntity<?> addDirector(@RequestBody Director director) throws ParseException {
//
//        Director savedDirector = directorService.createDirector(director);
//        return ResponseEntity.status(HttpStatus.OK).body(savedDirector);
////        return directorRepository.save(director);
//    }

//    @PutMapping("/updateDirector/{directorId}")
//    public ResponseEntity<Director> updateDirector(@PathVariable Long directorId, @RequestBody Director director) throws ParseException {
//        Director checkExist = directorRepository.findById(directorId).orElse(null);
////        System.out.println(checkExist);
//        if (checkExist == null) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        } else {
//            Director updateDirector = directorService.updateDirector(checkExist, director);
//            return ResponseEntity.status(HttpStatus.OK).body(updateDirector);
//        }
//    }

//    @DeleteMapping("/deleteDirector/{directorId}")
//    public ResponseEntity<String> deleteDirector(@PathVariable("directorId") Long directorId) throws ParseException {
//        Director director = directorRepository.findById(directorId).orElse(null);
//        if (director == null) {
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Director with id " + directorId + " not found");
//        } else {
//            directorService.deleteDirector(director);
//            return ResponseEntity.ok("Delete success with director id: " + directorId);
//        }
//    }
}
