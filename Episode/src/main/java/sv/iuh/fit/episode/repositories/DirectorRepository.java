package sv.iuh.fit.episode.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sv.iuh.fit.episode.Models.Director;
@Repository
public interface DirectorRepository extends JpaRepository<Director, Long> {
}
