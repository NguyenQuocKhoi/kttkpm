package sv.iuh.fit.authjwt.repository;

import sv.iuh.fit.authjwt.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
