package sv.iuh.fit.authjwt.entity;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;
//@Entity
//@Table(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Profile implements Serializable {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String firstName;
    private String lastName;
    private String username;
    private boolean gender;
    private LocalDate dob;
}
